/usr/local/bin/python3 manage.py migrate
/usr/local/bin/python3 manage.py makemigrations --noinput
/usr/local/bin/python3 manage.py migrate
/usr/local/bin/python3 manage.py collectstatic --noinput
echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.filter(username='admin').exists() or User.objects.create_superuser('admin', 'admin@domain.com', 'P@ssw0rd')" | python3 manage.py shell
/usr/local/bin/gunicorn docker_django.wsgi:application -w 2 -b :8000
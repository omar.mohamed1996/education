### Docker Instructions / Run
1. Build images - `docker-compose build`
2. Start services - `docker-compose up`

### Local Instructions / Run
1. `virtualenv venv -p python3`
2. `source venv/bin/activate`
3. `pip install -r ./web/requirements.txt`
3. `python web/manage.py migrate`
4. `python web/manage.py runserver`
